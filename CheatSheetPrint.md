# Non-Fiction Cheat Sheet
### Clone or download `https://gitlab.com/DomesticScience/InteractiveNonFiction` 
### Go to `http://pinboard.in/u:cheapjack/t:InF/` 

## Git
-- Useful for finding resources of all kinds, allows you to clone, track, manage and change projects.
`https://rogerdudler.github.io/git-guide/`

## Command line
#### Resources 
`http://linuxcommand.org/`
`https://github.com/veltman/clmystery`
-- Windows users check out `https://cygwin.com/` to get these tools and our guide in the repo 

### `man`
-- Get full info on command line commands with 
```
$ man commmand_I_want_to_know_about
```

### `grep` 
-- searches any given input file(s), selecting lines that match one or more patterns.

```
# Find all occurrences of 'patricia' in a file:
$ grep 'patricia' myfile
# Find all lines that don't include (-v) 'London', recursively (-R) in the 
# CityDirectory & ignore case (-i) (grep is case sensitive by default) &
# save to a new file called NotLondon.txt in the current directory
$ grep -v -R -i 'London' CityDirectory > NotLondon.txt
```


### `uniq` 
-- report or filter out repeated lines in a file

```
uniq -u {{file}}    # Display only unique lines 
uniq {{file}}       # Display each line once 
uniq -c {{file}}   # Display number of occurences of each line along with that line.
```

### `sort`
-- sort lines of text files

```
$ sort -nr > sorted_file_list.txt
```

## Twine
#### Resources
`http://www.motoslave.net/sugarcube`
`http://twine2.neocities.org`

### General markup of text
Similar to `git` markdown, Twine handles formatting text a certain way to output html for text display in browser.

Styling|Markup code|Result|HTML produced
---------------------|---------- | ---------------------- | ------------------ 
Italics | `//text//` | *text* | `<i>text</i>`
Boldface | `''text''` | **text** | `<b>text</b>`
Deleted/spoiler text | `~~text~~` | ~~text~~ | `<del>text</del>`
Emphasis | `*text*` | *text* | `<em>text</em>`
Strong emphasis | `**text**` |  **text** | `<strong>text</strong>`
Superscript | `meters/second^^2^^` | meters/second^2^ | `meters/second<sup>2</sup>`

### `[[New_Passage|The_New_Passage]]`
-- Matching non-nesting pairs of `[[` and `]]`, link text and the passage name, separated by a 'pipe' `|`.

```
[[Go to the cellar|Cellar]]
<!-- is a link that goes to a passage named "Cellar".-->
```

### `<<set expression>>` `(set: expression)`
-- Stores data (numbers, strings, arrays) values in variables.

```
<<set $Variable to VariableValue>>
<<set $Variable == VariableValue>>
(set: $Variable to [VariableValue])
(set: $Variable == [VariableValue])
``` 

### `<<print expression>>` `(print: expression)`
-- prints out any single argument provided to it, as text.

```
<<print $gold>>
(print: $var + "s")
```

#### `<<if>> (if: )[ ]` & `<<elseif>> (else-if: )[ ]`

`if` `else` and `elseif` logic in twine

```
<<set $legs to 8>>
(set: $legs to 8)
<<if: $legs is 8>>You're a spider!
(if: $legs is 8)[You're a spider!]

<<if $affection eq 75>>
    I love you!
<<elseif $affection == 50>>
    I like you.
<<elseif $affection == 25>>
    I'm okay with you.
<<else>>
    Get out of my face.
<</if>>
```
  
## Javascript

### Resources
`https://pinboard.in/u:cheapjack/t:tutorials/t:InF/t:workshops/t:mozilla` from `moz://a` written by Chris Mills
    
### `XMLHttpRequest`
-- Older technology to make simple requests for data, these days as JSON
       
```
var request = new XMLHttpRequest();
request.open('GET', url);
request.responseType = 'text';
request.send();
```

### `fetch()`
-- modern replacement for XHR introduced in browsers to make `asynch` HTTP requests easier to do in JavaScript. Refer to the `moz://a` MDN web docs from links above

```
// invoke the fetch() method passing url we want
// returns a 'promise', which resolves to the response sent back from the server, .then() runs code after you run the 
// response.text() method
fetch(url).then(function(response) {
    response.text().then(function(text) {
        poemDisplay.textContent = text;
        });
    });
```

### `Asynchronous Requests & security`
-- Most browsers won't allow `async` requests locally for security reasons
`https://developer.mozilla.org/en-US/docs/Learn/Server-side/First_steps/Website_security`

### `SimpleHTTPServer`
-- Workaround the `async` issue using `python`'s `SimpleHTTPServer` to serve the files on a local webserver, serving from `localhost:8000` in your web browser.

```
# On Mac and Linux
python -m SimpleHTTPServer
# On Windows
python -m http.server
```


# Interactive Non Fiction

<img src="images/makingInF.png" width="400">

Tools, tutorials, repository and portal for Domestic Science's  [Interactive Non-Fiction Professional Workshops](http://domesticscience.org.uk/InF.html#CPD)

We'll be making some very basic introductions likening `html` to a *choose your own adventure*. Then we'll have some **adventures in text** with *command line tools*, learn to eyeball `JSON` files for a data adventure and make our own files for simple interactive artworks and games and look at further resources like `node-red` to build on.

## [Tocky Tales](http://planning.mcqn.com/)

As an example of what you can do check out [Adrian McEwen's](https://twitter.com/amcewen) example game ***Tocky Tales*** he made at our workshop using [node-red](https://nodered.org/), [Twine](http://twinery.org) and **Dave Mee**'s [TwineNodered](http://github.com/davemee/TwineNodered) code. Follow your fantasy planning votes from the game on Twitter [@ToxtethPlanning](https://twitter.com/ToxtethPlanning) and [find out more here](http://mcqn.com/ibal192)

## Getting Started

It's not essential but you might like to look at some of the [basic resources at Mozilla here](https://developer.mozilla.org/en-US/docs/Learn) made by the awesome technical writer **Chris Mills**. We are only expecting basic familiarity with html, the command line and things like javascript or python so you *can* come along with just basic knowledge.

It's also worth looking at [this](https://www.mozilla.org/en-US/internet-health/) and we'll be doing some playing with [node-red](https://nodered.org/) and [Twine](http://twinery.org) thanks to **Dave Mee**'s [TwineNodered](http://github.com/davemee/TwineNodered) code.

We also advise playing the [Command Line Murder Mystery](https://github.com/veltman/clmystery) to get practice on using the command line to pull out useful things from large text files! 

Then you can dive into Adrian's [Adventures In Text](https://gitlab.com/DomesticScience/InteractiveNonFiction/tree/master/chooseYourOwnAdventure/adventuresInText) resources that helped make **Tocky Tales**! 

## Caveats

We are going to assume only very basic knowledge of using the Web here; many of those attending will have more than basic knowledge so please bear with us and indeed challenge us. Contrary to what people may expect we won't be doing much **coding**; we will be doing alot of reading and talking, some writing, typing and manipulating data. So we won't be teaching you to be software developers, but you may find talking to developers in the future alot easier.

This sounds cryptic but it will all make sense.

Approaching very simple things again may seem frustrating but it is valuable. That is our overall caveat; dont be insulted; re-doing the basics even if you know them implicitly helps learning, access, literacy, dialogue and in the end creativity. 

## Contributors

 * [Dave Mee](https://twitter.com/davemee) from the [garden](http://thegarden.io/) and his [TwineNodered](http://github.com/davemee/TwineNodered) project
 * [Adrian McEwen](https://twitter.com/amcewen) from [MCQN Ltd](http://mcqn.com/) for mentoring and course materials and the [Cocklecraft-of-things](https://github.com/mcqn/cocklecraft-of-things)
 * [Ross Dalziel](http://cheapjack.org.uk) for general technical editor & curation simple opinionated `html` & chatty vanilla Twine 2.1 games.
 * [Chris Huffee](http://huffee.com) with his [The Software Developer Who Cried Wolf](#) example Twine game

## Technical Notes

You might save yoursef time and if you are bringing along your own laptop you could install a few useful things prior to joining us.

 * Install [node.js](https://nodejs.org/en/download/)
 * Install [node-red](https://nodered.org/)
 * Install the latest [Twine](http://twinery.org/)

### Notes for Windows Users

We'll be using [command line tools](http://linuxcommand.org/) to help manage the accessible data for our work, which is simple on Linux and Mac OS as they've got them built in. 

If you are a regular windows user we suggest installing [Cygwin](https://www.cygwin.com/) to use powerful command line tools that Windows does not quite provide. We've made it easy to install locally on the day so please [refer to these install notes](https://gitlab.com/DomesticScience/InteractiveNonFiction/blob/master/cygwin/README.md). 

An alternative is using something like [Cash](https://github.com/dthree/cash) but this doesn't have all of the useful `UNIX`-like commands we might need but we thought we'd mention it here. It requires you to install [node.js](https://nodejs.org/en/download/) which you'll need for playing with node-red anyway.


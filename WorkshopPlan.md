
# Workshop Plan
Initial Workshop Plan

|Time|Activity|Leaders|Notes
|----|----|----|----
09:00|Setup|Ross/Denise/Adrian|Setup laptops, signage, arcade cab
10:00|Introductions|Ross|Background, what we want
10:30|Choose Your Own `html` Adventure|Ross|Do the [example](https://gitlab.com/DomesticScience/InteractiveNonFiction/tree/master/chooseYourOwnAdventure) in a text editor and show it
11:00|Adventures-in-text|Ross, Adrian|grep-eyeballing-data, Cleaning up data with grep/cat/sort, API-DIY 
12:00|Twine Show & Play|All|Show Wray Castle, INF games, Beba-Me, cocklecraft-of-things
12:15|Twine Basics|Ross|Make basic games with Twin
12:30|Lunch|All|Squash Nutrition on Friday, Some basic sandwich materials & water Saturday 
13:00|Twine with data, jQuery & javascript|Ross, Adrian|Introduce simple twine shim for people to make, use our data from Adventures-in-text
13:30|Twine & Node-Red|Dave/Adrian, Ross|Introduce [twineNodeRed](http://github.com/davemee/TwineNodered) and build things.
15:00|Other approaches|All|Static Websites like [Has Theresa Resigned Yet?](http://hastheresamayresignedyet.com/), [Idyll](http://idyll-lang.org/), [Inform](http://inform7.com/)




# `node-red`

## `node-red` WTF?

[Node-red](https://nodered.org) is an easy to use UI (user interface) for programming the Internet of Things and all kinds of other network interactions

It's something you can easily install on a linux webserver, your own computer for prototyping or a Raspberry Pi and leave it running.


It's super easy and more sustainable to just run on a Raspberry Pi and leave it on your home or office network; for a small project this is more than enough but you might have to consider security.

I made an example node-red instance on an Ubuntu server on a [Digital Ocean Droplet](https://www.digitalocean.com/community/tutorials/how-to-create-your-first-digitalocean-droplet) in about half an hour after following [their tutorial](https://www.digitalocean.com/community/tutorials/how-to-connect-your-internet-of-things-with-node-red-on-ubuntu-16-04) 

Digital Ocean are a simple cloud computing provider; they've got a tonne of contemporary tutorials there and if you end up needing to scale what you are doing they can easily manage it, for a price. It's all setup for Linux and controlling it by ssh which is essential for node-red setups and Beyoncé hosts her albums on it! (Actually true)

We'll go through how node-red works and run through an example flow and there's some links to tutorials elsewhere here. 

Node-red is based on ‘flows’. A flow is a window in the node-red interface where you setup and arrange all the components and ‘flow’ of the data and services you want to arrange.

This could be an hourly read of a Twitter hashtag to update a webpage, or listening for a button press to trigger an email. Doing something like this might be trivial to most developers or programmers but daunting for a novice or intermediate enthusiast. 

What's more there's a tonne of other flows in the [Flow Library](https://flows.nodered.org/) you can begin using in your work.

`node-red` flows help you do this easily: once you test and deploy your arrangement of buttons and data flows you can make this kind of interaction happen easily

## TwineNodered

We're super lucky that Dave Mee has been hacking Twine so it works really easily with node-red.

As we saw earlier you can insert JavaScript into a passage in a Twine story and display JSON fairly easily, even providing some kind of logic regarding what is displayed in the passage.

However it only ever lives in that passage at the instance you load it. Once you go away to another passage in the game you can't really access the same data without running the JavaScript again which is a new instance basically.

Dave's hack let's us make flows that affect Twine variables globally in the games we make which is far more useful for building interactive stories and games in Twine: it also could mean changes to game variables could affect outside services and events managed by Twine. Essentially Twine remembers data served to it by node-red for you to play with.

Even better it lets you host the twine game in the node-red space

First download the [twineNodeRed](https://github.com/davemee/twineNodeRed) repo with `$ wget https://github.com/davemee/twineNodeRed.zip` or with `$ git clone https://github.com/davemee/twineNodeRed`

Now `$ cd twineNodeRed` and fire up node-red with `$ node-red`. You might find `screen` command useful here so you can run it and then 'detach' from it and do other things.

## Requirements

Once again windows users should refer to @amcewen 's notes on [installing cygwin](https://gitlab.com/DomesticScience/InteractiveNonFiction/tree/master/cygwin) and refer to the install instructions below

 * You install node-red with [npm](https://www.npmjs.com/get-npm?utm_source=house&utm_medium=homepage&utm_campaign=free%20orgs&utm_term=Install%20npm) the package manager which comes with [Node.js](https://nodejs.org/en/)
 * [node-red](https://nodered.org/docs/getting-started/installation.html) is installed like this.

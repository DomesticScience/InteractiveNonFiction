We've downloaded and saved the contents of the three sessions of the main debate.  First step is to combine them all into one file so we work on it all at once
```
  cat InvestigatoryPowersBill-20160* > IPBillCombined
```
So `cat` outputs the contents of all the files mentioned after it to the terminal, but `> IPBillCombined` redirects all of that output into a file called "IPBillCombined"

From a look at the contents of the web pages we downloaded, and having a hunt through for one of the MP's names, it looks like the `<h2 class="memberLink">` would be a useful thing to focus on.  It's possible that MPs might be linked in the text of the speech if they're mentioned, for example, or maybe in the header or footer of the page, so looking for the `h2` elements is less likely to pick up any mentions that *aren't* the intro to that person speaking.  (The more you do this, the better you'll get at spotting what's a sensible thing to look for)

This will pull out all of the lines in the file which include the text "memberLink":
```
  grep memberLink IPBillCombined 
```

However, if you run that you'll see it's just a load of lines like this:

>               <h2 class="memberLink">
>               <h2 class="memberLink">
>               <h2 class="memberLink">
>               <h2 class="memberLink">

That's not too useful, but when eyeballing the HTML we see that it's actually the line _after_ the `h2` that's of interest.  Let's get `grep` to include that too by adding `--after-context=1`


```
  grep memberLink IPBillCombined --after-context=1
```

On my computer that seems to be missing the stuff we're actually after, so let's try getting the second line after the `h2` too.
```
  grep memberLink IPBillCombined --after-context=2
```

> --
>                <h2 class="memberLink">
>                    <a class="nohighlight" href="/search/MemberContributions?house=Commons&amp;memberId=4475"
>                       title="View member's contributions">Suella Fernandes</a>
> --
>                <h2 class="memberLink">
>                    <a class="nohighlight" href="/search/MemberContributions?house=Commons&amp;memberId=1540"
>                       title="View member's contributions">Greg Mulholland (Leeds North West) (LD)</a>

Ok.  That has a load of MP names in the output now.  However, it also has a lot of lines that we're not interested in.  We can use the "pipe symbol" `|` in order to take the output from one command and feed it into another command.

The `-v` parameter for `grep` will invert the match, so rather than outputting lines that match the string you give it, it will output all the lines that _don't'_ match the string you give it.

So, if we pipe the output from the first `grep` command through a number of additional `grep -v` commands we can strip out...

```
  grep memberLink IPBillCombined --after-context=2 | grep -v nohighlight
```
...the "nohighlight" lines, and then...

```
  grep memberLink IPBillCombined --after-context=2 | grep -v nohighlight | grep -v memberLink
```
...the `h2` lines.

That leaves the "--" separator lines.  They're a tiny bit trickier to remove.

```
  grep memberLink IPBillCombined --after-context=2 | grep -v nohighlight | grep -v memberLink | grep -v --
```
The obvious thing to try, as above, will result in a message about how to use `grep`.  That's because it thinks the `--` is the start of a parameter to pass to grep, like at the beginning of `--after-context=2`.

In this scenario, enclosing the offending characters in single quotes '' can often do the trick.

```
  grep memberLink IPBillCombined --after-context=2 | grep -v nohighlight | grep -v memberLink | grep -v '--'
```

Nope.  Still no joy.  Generally in that case you can "escape" the offending characters by prefixing them with a backslash "\"

```
  grep memberLink IPBillCombined --after-context=2 | grep -v nohighlight | grep -v memberLink | grep -v '\-\-'
```

That did the trick.  Now we've only got lines containing MPs' names, albeit with a bit of HTML cruft round them still.

>                      title="View member's contributions">Victoria Atkins</a>
>                      title="View member's contributions">Mr Carmichael</a>
>                      title="View member's contributions">Victoria Atkins</a>
>                      title="View member's contributions">Dr Murrison</a>
>                      title="View member's contributions">Mr Carmichael</a>


To tidy that up we'll deploy the command `cut`.  That allows you to split up on a character of your choice and cherry-pick the parts you want.

We can use this pretty simplistically to discard everything up to the ">".  Providing the `-d \>` parameter (note the `\` to escape the `>` otherwise it will try to direct the output to a file) we can split each line of input where there's a `>` character, and the adding the parameter `-f 2` we can choose the second field (so everything _after_ the `>` rather than everything _before_ it)

```
  grep memberLink IPBillCombined --after-context=2 | grep -v nohighlight | grep -v memberLink | grep -v '\-\-' | cut -d \> -f 2
```

Now we can perform the same trick to trim off the "</a" at the end of the line.

```
  grep memberLink IPBillCombined --after-context=2 | grep -v nohighlight | grep -v memberLink | grep -v '\-\-' | cut -d \> -f 2 | cut -d \< -f 1
```

That gives us a list of names!  Now to find out how many times each one spoke.

First off we should group together all of the times each person spoke.  We can do that with a simple `sort`.

```
  grep memberLink IPBillCombined --after-context=2 | grep -v nohighlight | grep -v memberLink | grep -v '\-\-' | cut -d \> -f 2 | cut -d \< -f 1 | sort 
```

Now we can use `uniq` to combine any repeated names, and if we add the `-c` parameter it will also print out how many times each one was repeated.

```
  grep memberLink IPBillCombined --after-context=2 | grep -v nohighlight | grep -v memberLink | grep -v '\-\-' | cut -d \> -f 2 | cut -d \< -f 1 | sort | uniq -c
```

That shows that there's a bit more data-cleaning to do, ideally, because the first time each MP is mentioned it also includes details of their seat.  There are also slight differences in wording occasionally - for example, "Ms Harriet Harman" is abbreviated to "Ms Harman" after the initial mention.

Ignoring the changes in name - they'll need to be caught by hand - we can back up a step and use an intermediate file and text editor to clean the seat.

```
  grep memberLink IPBillCombined --after-context=2 | grep -v nohighlight | grep -v memberLink | grep -v '\-\-' | cut -d \> -f 2 | cut -d \< -f 1 | sort > MPcount
```

The `> MPcount` will redirect all of the text into a file called MPcount.  Then we can open that in `vim`.  This is pretty advanced usage now, but hopefully will show the sort of power you can wield if you get to grips with some of the less-user-friendly text editors (and maybe explain why the hardcore geeks are so wedded to them!)

```
  vim MPcount
```

Then in `vim` type `:g/ (/s/ (.*//`.  That looks at all the lines in the file that contain " (" somewhere in them - the `:g/ (/` bit.  Then on each of those lines, it performs a search and replace - the `s/find-something-that-matches-this/replace-that-with-whatever-is-here/`.  It finds the part that matches " (" and all the characters that follow it ".*" ("." matches *any* character, and "*" means match as many as you can find), and replaces it with nothing.

Now to save and exit `vim` you need to type `:wq`.

The MPcount file will now submit to us using `uniq -c` on it, and a final run through `sort` with the `-n` parameter will sort the names in numeric order so we can easily see who spoke the most often.

```
  cat MPcount | uniq -c | sort -n
```

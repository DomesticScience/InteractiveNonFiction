# Adventures in Text: Grep-Eyeballing and cleaning up data with command-line tools

Although data comes in many different formats, a lot of those formats are basically a specialised version of a text file.  So, sometimes it's as easy to treat them at the lowest-common-denominator and wield a set of tools to manipulate, slice, dice and search text.

This folder contains a number of [text-based datasets](datasets) for you to experiment with, and a few worked examples to give you some ideas:

 * [Find out who spoke and how often in a House of Commons debate](Hansard.md)
 * [Analyze the companies working in the Internet of Things in the UK](IoTUK.md)
 * [Turning a CSV file into JSON](CSVtoJSON.md)


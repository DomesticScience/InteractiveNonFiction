<!DOCTYPE html>
<!--[if IE 7]> <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width"/>

    <meta name="description" content="Hansard (the Official Report) is the edited verbatim report of proceedings of both the House of Commons and the House of Lords. Daily Debates from Hansard are published on this website the next working day.">
    
    <link rel="dns-prefetch" href="//ajax.googleapis.com/"/>
    <link rel="shortcut icon" href="/Assets/img/favicon.ico" type="image/x-icon" />
    
        <title>Investigatory Powers Bill (Programme) (No. 2) - Hansard Online</title>

    <link href="/assets/css/bundle?v=DYMIkxDxHyyTleHRGmZx-5TF6hdpzZ8SBobyBdy_nME1" rel="stylesheet"/>

    <script src="/assets/js/modernizr/bundle?v=4Dmh6ovjssOKPw9dZqg0U8FgOCXLJMRqYqL4wv9UEJM1"></script>



</head>

<body>

<div class="alert alert-warning disclaimer" id="data-disclaimer">
    <div class="container">
        <div class="alert-inner"><p>
        <span class="glyphicon glyphicon-warning-sign"></span><strong>Cookies: </strong>We use cookies to give you the best possible experience on our site. By continuing to use the site you agree to our use of cookies. <a href="http://www.parliament.uk/site-information/privacy/">Find out more.</a>
            <button type="submit" class="btn btn-warning">OK</button>
        </div>
    </div>
</div>
<div id="wrapper">
    <div class="container">
        <div id="header">
            <div class="row">
                <div hidden="hidden" id="process-warning" class="alert alert-warning" data-processing-url="/processing/getcurrentlyprocessing">
                    <span class="glyphicon glyphicon-warning-sign"></span><span id="process-warning-message"></span>
                </div>

                <div class="col-md-12">
                    <div class="site-title pull-left">
                        <img id="mainImage" src="/Assets/img/parliament-uk-logo.gif" alt="Parliament logo">
                    </div>
                    <a target="_blank" id="mainHomeLink" href="http://www.parliament.uk">www.parliament.uk</a>
                    <div id="websiteNavigationLinks" class="pull-right">
                        <a target="_blank" href="http://www.parliament.uk/business/"><span class="hidden-sm hidden-xs"><strong>Parliamentary Business</strong></span><span class="hidden-xs hidden-md hidden-lg"><strong>Business</strong></span></a><span class="hidden-xs"> |</span>
                        <a target="_blank" href="http://www.parliament.uk/mps-lords-and-offices/"><span class="hidden-sm hidden-xs">MPs, Lords and Offices</span><span class="hidden-xs hidden-md hidden-lg">Members</span></a><span class="hidden-xs"> |</span>
                        <a target="_blank" href="http://www.parliament.uk/about/"><span class="hidden-sm hidden-xs">About Parliament</span><span class="hidden-xs hidden-md hidden-lg">About</span></a><span class="hidden-xs"> |</span>
                        <a target="_blank" href="http://www.parliament.uk/get-involved/"><span class="hidden-xs">Get Involved</span></a><span class="hidden-xs"> |</span>
                        <a target="_blank" href="http://www.parliament.uk/visiting/"><span class="hidden-xs">Visiting</span></a><span class="hidden-xs"> |</span>
                        <a target="_blank" href="http://www.parliament.uk/education/"><span class="hidden-xs">Education</span></a>
                    </div>

                </div>
            </div>

            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/"><span class="glyphicon glyphicon-home home-icon"></span>Hansard Online</a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Commons <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/Commons/latestsittingday">Latest Sitting</a></li>
                                    <li><a href="/Commons">Browse Sittings</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="/search/Debates?house=commons">List Debates</a></li>
                                    <li><a href="/search/Divisions?house=commons">List Divisions</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="/search/Members?house=commons&amp;currentFormerFilter=1">Browse MPs</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Lords <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/Lords/latestsittingday">Latest Sitting</a></li>
                                    <li><a href="/Lords">Browse Sittings</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="/search/Debates?house=lords">List Debates</a></li>
                                    <li><a href="/search/Divisions?house=lords">List Divisions</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="/search/Members?house=lords&amp;currentFormerFilter=1">Browse Peers</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Archives <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a target="_blank" href="http://www.parliament.uk/business/publications/hansard/commons">Commons Archive</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a target="_blank" href="http://www.parliament.uk/business/publications/hansard/lords">Lords Archive</a></li>                         
                                </ul>
                            </li>
                            <li><a href="/about">About</a></li>
                        </ul>
                    
<form action="/search" class="navbar-form navbar-right" method="post" role="search">                            <div class="search-form">
                                <input id="SearchTermTop" name="SearchTerm" type="text" class="form-control" placeholder="Search Hansard...">
                                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                            </div>
</form>                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>

            <div class="to-top-link">
                <div id="top-link-block" class="hidden">
                    <a href="#" class="well well-sm hidden" id="previous-top-link"><i class="glyphicon glyphicon-chevron-left"></i> Previous</a>
                    <a href="#header" class="well well-sm" id="back-to-top-link"><i class="glyphicon glyphicon-chevron-up"></i> Top</a>
                    <a href="#" class="well well-sm hidden" id="next-top-link"><i class="glyphicon glyphicon-chevron-right"></i> Next</a>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div id="main" class="content">
            

<div class="row">

    <div class="col-lg-12">

        <!-- House coloured header bar -->
        <div class="commons house-header col-xs-12">
            <span>House of Commons <strong>Hansard</strong></span>
        </div>

        

<div class="row content debate-content">
    <div class="col-xs-12">

        <!-- the breadcrumb is to navigate around the day and go back to the date picker-->
        <ol class="breadcrumb hidden-xs">
            <li>
                <a href="/Commons/2016-06-06">
                    <strong>Contents</strong>
                </a>

            </li>

                <li>
                        <a href="/Commons/2016-06-06/debates/3fc9e2d7-2032-4d79-a640-82ca7faa26e3/CommonsChamber">
                            <span class="commons">Commons Chamber</span>
                        </a>
                </li>

            <li>
                <div class="hidden-sm download-as-text">
                    <a class="link-text" href="/debates/GetDebateAsText/16060613000002"><span class="glyphicon glyphicon-file"></span> Text only</a>
                </div>
                
            </li>
        </ol>

        <div class="breadcrumb visible-xs">
            <a class="btn btn-wide" href="/Commons/2016-06-06">
                Back to Contents
            </a>
        </div>

    </div>


    <!--the header is the back/forward navigation and the title of the debate-->

    <div class="col-xs-12 header">
        <div class="col-xs-2 left-arrow">
                <a href="/Commons/2016-06-06/debates/1606066000002/PointsOfOrder" class="previous" title="Go to the previous debate">

                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
        </div>
        <div class="col-xs-8">
            <h1 class="page-title">Investigatory Powers Bill (Programme) (No. 2)</h1>
        </div>

        <div class="col-xs-2 right-arrow">
                <a href="/Commons/2016-06-06/debates/16060613000001/InvestigatoryPowersBill" class="next" title="Go to the next debate">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
        </div>

            <div class="col-xs-12">
                <div class="share-debate">
                    <a href="#debate-177764" rel="popover" class="link-to-contribution link-text" title="Share this debate" data-placement="top"
                       data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                       data-hop-popover="http://hansard.parliament.uk/Commons/2016-06-06/debates/16060613000002/InvestigatoryPowersBill(Programme)(No2)">
                        <div class="share-icon">&nbsp;</div>&nbsp;Share this debate
                    </a>
                </div>
            </div>
        

    </div>
 
    <div class="col-xs-12 debate-date">06 June 2016</div>

        <div class="col-xs-12 debate-date">Volume 611</div>

    <div class="col-xs-12 col-md-12">
        <div id="debateContent">
            <div class="highlightedSearchTerms"></div>
            <ul>
<!-- START statement -->
<div class="statement col-md-9 hs_ColumnNumber" style="padding-left: 0px;">
        <span data-toggle="tooltip" data-hop-debug-tooltip class="glyphicon glyphicon-info-sign hidden hidden-xs" data-placement="bottom" title=" hs_ColumnNumber "></span>

<p class="hs_ColumnNumber"><span id="868" class="column-number column-only" data-column-number="868"></span></p>
</div>

<!-- END statement -->
<div class="col-md-3 right-column"></div>
<!-- START timestamp -->
<div class="col-md-9" style="padding-left: 0px;">
    <p class="timestamp">
        <time datetime="05/10/2017 17:01:00"><span class="glyphicon glyphicon-time"></span> 
5.01 pm</time>
    </p>
</div>
    <!-- END timestamp -->
    <li id="contribution-72328B8A-3605-488F-B73E-CBA738B81E76">

            <div class="col-md-9" style="padding-left: 0px;">
                <h2 class="memberLink">
                    <a class="nohighlight" href="/search/MemberContributions?house=Commons&amp;memberId=350"
                       title="View member's contributions">The Minister for Security (Mr John Hayes)</a>
                </h2>
            </div>
                <div class="col-md-3 hidden-sm hidden-xs" style="padding-left: 0px;">
                    <a href="#contribution-72328B8A-3605-488F-B73E-CBA738B81E76" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
                       data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                       data-hop-popover="http://hansard.parliament.uk/Commons/2016-06-06/debates/16060613000002/InvestigatoryPowersBill(Programme)(No2)#contribution-72328B8A-3605-488F-B73E-CBA738B81E76">
                        <div class="share-icon">&nbsp;</div><span class="share-text">&nbsp;Share this contribution</span>
                    </a>
                </div>

            <span data-toggle="tooltip" data-hop-debug-tooltip class="glyphicon glyphicon-info-sign hidden hidden-xs" data-placement="bottom" title="BE2-BJ1 hs_Para 06/06/2016 17:06:06"></span>



        <div class="inner">
            <div class="contribution col-md-9">
<p class="hs_Para">I beg to move,</p><p class="hs_Para"></p><p class="hs_Para">That the Order of 15 March 2016 (Investigatory Powers Bill (Programme)) in the last session of Parliament be varied as follows:</p><p class="hs_Para"></p><p class="hs_Para">(1) Paragraphs (5) and (6) of the Order shall be omitted.</p><p class="hs_Para"></p><p class="hs_Para">(2) Proceedings on Consideration shall be taken on the days and in the order shown in the first column of the following Table.</p><p class="hs_Para"></p><p class="hs_Para">(3) The proceedings shall (so far as not previously concluded) be brought to a conclusion at the times specified in the second column of the Table.</p><p class="hs_Para"></p><p class="hs_Para"><table class="hansardTable"><thead><tr><th><p>Table</p></th></tr></thead><tbody><tr><td><p><em>Proceedings</em></p></td><td><p><em>Time for conclusion of  proceedings</em></p></td></tr><tr><td><p>Day 1</p></td><td></td></tr><tr><td><p>New Clauses and new Schedules relating to, and amendments to, Part 1; new Clauses and new Schedules relating to, and amendments to, Part 8</p><p>New Clauses and new Schedules relating to, and amendments to, Part 2; new Clauses and new Schedules relating to, and amendments to, Part 5; new Clauses and new Schedules relating to, and amendments to, Chapter 1 of Part 9</p></td><td><p>Three hours after the commencement of proceedings on the Motion for this Order</p><p>Six hours after the commencement of proceedings on the Motion for this Order</p></td></tr><tr><td><p>Day 2</p></td><td></td></tr><tr><td><p>New Clauses and new Schedules relating to, and amendments to, Part 6; new Clauses and new Schedules relating to, and amendments to, Part 7</p><p>New Clauses and new Schedules relating to, and amendments to, Part 3; new Clauses and new Schedules relating to, and amendments to, Part 4; new Clauses and new Schedules relating to, and amendments to, Chapter 2 of Part 9; remaining proceedings on Consideration</p></td><td><p>Three hours after the commencement of proceedings on Consideration on the second day</p><p>One hour before the moment of interruption</p></td></tr></tbody></table></p><p class="hs_Para"></p><p class="hs_Para">(4) Any proceedings in Legislative Grand Committee and proceedings on Third Reading shall (so far as not previously concluded) be brought to a conclusion at the moment of interruption on the second day.</p><p class="hs_Para"></p><p class="hs_Para">I am immensely grateful to you, Mr Speaker, for the opportunity to move the programme motion. I do not want to delay the House unduly, because there are many significant matters to debate in this important legislation. It has been the Government’s habit, in respect of the Bill, to engage in the most careful—</p>                        <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                            <a href="#contribution-72328B8A-3605-488F-B73E-CBA738B81E76" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
                               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                               data-hop-popover="http://hansard.parliament.uk/Commons/2016-06-06/debates/16060613000002/InvestigatoryPowersBill(Programme)(No2)#contribution-72328B8A-3605-488F-B73E-CBA738B81E76">
                                <div class="share-icon">&nbsp;</div>&nbsp;Share this contribution
                            </a>
                        </div>

            </div>
            <div class="col-md-3 right-column">

            </div>
            <div class="clearfix"></div>
        </div>
    </li>

    <li id="contribution-FEFDC28E-5B61-4B07-B289-580C2632BEF3">

            <div class="col-md-9 nohighlight" style="padding-left: 0px;">
                Hon. Members
            </div>

            <span data-toggle="tooltip" data-hop-debug-tooltip class="glyphicon glyphicon-info-sign hidden hidden-xs" data-placement="bottom" title="BE2-BJ1 hs_Para 06/06/2016 17:06:06"></span>



        <div class="inner">
            <div class="contribution col-md-9">
<p class="hs_Para"><strong> </strong>Formally!</p>
            </div>
            <div class="col-md-3 right-column">

            </div>
            <div class="clearfix"></div>
        </div>
    </li>

<!-- START statement -->
<div class="statement col-md-9 hs_ColumnNumber" style="padding-left: 0px;">
        <span data-toggle="tooltip" data-hop-debug-tooltip class="glyphicon glyphicon-info-sign hidden hidden-xs" data-placement="bottom" title=" hs_ColumnNumber "></span>

<p class="hs_ColumnNumber"><span id="869" class="column-number column-only" data-column-number="869"></span></p>
</div>

<!-- END statement -->
<div class="col-md-3 right-column"></div>
    <li id="contribution-38DADD9A-8881-4DE6-95EF-9769A3328142">

            <div class="col-md-9" style="padding-left: 0px;">
                <h2 class="memberLink">
                    <a class="nohighlight" href="/search/MemberContributions?house=Commons&amp;memberId=350"
                       title="View member's contributions">Mr Hayes</a>
                </h2>
            </div>
                <div class="col-md-3 hidden-sm hidden-xs" style="padding-left: 0px;">
                    <a href="#contribution-38DADD9A-8881-4DE6-95EF-9769A3328142" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
                       data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                       data-hop-popover="http://hansard.parliament.uk/Commons/2016-06-06/debates/16060613000002/InvestigatoryPowersBill(Programme)(No2)#contribution-38DADD9A-8881-4DE6-95EF-9769A3328142">
                        <div class="share-icon">&nbsp;</div><span class="share-text">&nbsp;Share this contribution</span>
                    </a>
                </div>

            <span data-toggle="tooltip" data-hop-debug-tooltip class="glyphicon glyphicon-info-sign hidden hidden-xs" data-placement="bottom" title="BE2-BJ1 hs_Para 06/06/2016 17:06:08"></span>



        <div class="inner">
            <div class="contribution col-md-9">
<p class="hs_Para">I have already excited my right hon. and learned Friend the Member for Beaconsfield (Mr Grieve), and I hope that I will continue to do so.</p>                        <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                            <a href="#contribution-38DADD9A-8881-4DE6-95EF-9769A3328142" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
                               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                               data-hop-popover="http://hansard.parliament.uk/Commons/2016-06-06/debates/16060613000002/InvestigatoryPowersBill(Programme)(No2)#contribution-38DADD9A-8881-4DE6-95EF-9769A3328142">
                                <div class="share-icon">&nbsp;</div>&nbsp;Share this contribution
                            </a>
                        </div>

            </div>
            <div class="col-md-3 right-column">

            </div>
            <div class="clearfix"></div>
        </div>
    </li>

    <li id="contribution-AD236114-B55B-4A48-9E8C-787378A80C1E">

            <div class="col-md-9" style="padding-left: 0px;">
                <h2 class="memberLink">
                    <a class="nohighlight" href="/search/MemberContributions?house=Commons&amp;memberId=17"
                       title="View member's contributions">Mr Speaker</a>
                </h2>
            </div>
                <div class="col-md-3 hidden-sm hidden-xs" style="padding-left: 0px;">
                    <a href="#contribution-AD236114-B55B-4A48-9E8C-787378A80C1E" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
                       data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                       data-hop-popover="http://hansard.parliament.uk/Commons/2016-06-06/debates/16060613000002/InvestigatoryPowersBill(Programme)(No2)#contribution-AD236114-B55B-4A48-9E8C-787378A80C1E">
                        <div class="share-icon">&nbsp;</div><span class="share-text">&nbsp;Share this contribution</span>
                    </a>
                </div>

            <span data-toggle="tooltip" data-hop-debug-tooltip class="glyphicon glyphicon-info-sign hidden hidden-xs" data-placement="bottom" title="BE2-BJ1 hs_Para 06/06/2016 17:06:23"></span>



        <div class="inner">
            <div class="contribution col-md-9">
<p class="hs_Para">I am not sure whether “excited” is correct; I think “irritated” might be, but in my experience the right hon. Gentleman has never let that put him off in the past.</p>                        <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                            <a href="#contribution-AD236114-B55B-4A48-9E8C-787378A80C1E" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
                               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                               data-hop-popover="http://hansard.parliament.uk/Commons/2016-06-06/debates/16060613000002/InvestigatoryPowersBill(Programme)(No2)#contribution-AD236114-B55B-4A48-9E8C-787378A80C1E">
                                <div class="share-icon">&nbsp;</div>&nbsp;Share this contribution
                            </a>
                        </div>

            </div>
            <div class="col-md-3 right-column">

            </div>
            <div class="clearfix"></div>
        </div>
    </li>

    <li id="contribution-FB19C1E0-B8D9-4504-AAD1-D57246CF0211">

            <div class="col-md-9" style="padding-left: 0px;">
                <h2 class="memberLink">
                    <a class="nohighlight" href="/search/MemberContributions?house=Commons&amp;memberId=350"
                       title="View member's contributions">Mr Hayes</a>
                </h2>
            </div>
                <div class="col-md-3 hidden-sm hidden-xs" style="padding-left: 0px;">
                    <a href="#contribution-FB19C1E0-B8D9-4504-AAD1-D57246CF0211" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
                       data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                       data-hop-popover="http://hansard.parliament.uk/Commons/2016-06-06/debates/16060613000002/InvestigatoryPowersBill(Programme)(No2)#contribution-FB19C1E0-B8D9-4504-AAD1-D57246CF0211">
                        <div class="share-icon">&nbsp;</div><span class="share-text">&nbsp;Share this contribution</span>
                    </a>
                </div>

            <span data-toggle="tooltip" data-hop-debug-tooltip class="glyphicon glyphicon-info-sign hidden hidden-xs" data-placement="bottom" title="BE2-BJ1 hs_Para 06/06/2016 17:06:29"></span>



        <div class="inner">
            <div class="contribution col-md-9">
<p class="hs_Para">And will certainly not do so in the next two days, Mr Speaker.</p><p class="hs_Para"></p><p class="hs_Para">The programme motion is relatively straightforward, because, as I was about to say, it is the Government’s habit, in respect of the Bill, to both listen and learn. Over the next two days, I hope to be able to show that we have done both. Scrutiny has been considerable, and the draft Bill that preceded the Bill that we are considering on Report was scrutinised closely by three parliamentary Committees, including a special Joint Committee, chaired and supported by Members of the Lords and the Commons, who gave the measure considerable attention. The Joint Committee produced a report with numerous recommendations, and members of the Public Bill Committee engaged in debate on those recommendations. There has therefore been a thorough process, and that will continue over the next two days.</p><p class="hs_Para"></p><p class="hs_Para"><em>Question put and agreed to.</em></p>                        <div class="hidden-md hidden-lg" style="padding-left: 0px;">
                            <a href="#contribution-FB19C1E0-B8D9-4504-AAD1-D57246CF0211" rel="popover" class="link-to-contribution link-text" title="Share this contribution" data-placement="top"
                               data-hop-url-shorten-url="/UrlShortener/ShorternUrl"
                               data-hop-popover="http://hansard.parliament.uk/Commons/2016-06-06/debates/16060613000002/InvestigatoryPowersBill(Programme)(No2)#contribution-FB19C1E0-B8D9-4504-AAD1-D57246CF0211">
                                <div class="share-icon">&nbsp;</div>&nbsp;Share this contribution
                            </a>
                        </div>

            </div>
            <div class="col-md-3 right-column">

            </div>
            <div class="clearfix"></div>
        </div>
    </li>

                            </ul>
        </div>
    </div>

    <div class="modal fade" id="divisionListModal">
    </div>
</div>

    </div>

</div>
        </div>
    </div>
    
    <div class="footer-bumper"></div>
    <p class="visible-xs"></p>
    <div class="footer">
        <footer class="container hidden-xs">
            <div class="text-muted">
                <a target="_blank" href="http://www.parliament.uk/site-information/azindex/">A-Z index</a> |
                <a target="_blank" href="http://www.parliament.uk/site-information/glossary/">Glossary</a> |
                <a target="_blank" href="http://www.parliament.uk/site-information/contact-us/">Contact us</a> |
                <a target="_blank" href="http://www.parliament.uk/site-information/foi/">Freedom of Information</a> |
                <a target="_blank" href="http://www.parliament.uk/site-information/data-protection/">Data Protection</a> |
                <span class="hidden-sm">
                    <a target="_blank" href="http://www.parliament.uk/site-information/job-opportunities/">Jobs</a> |
                    <a target="_blank" href="http://www.parliament.uk/site-information/using-this-website/">Using this website</a> |
                </span>
                <a target="_blank" href="http://www.parliament.uk/site-information/copyright/">Copyright</a>
            </div>
        </footer>
        <footer class="container hidden-sm hidden-md hidden-lg">
            <div class="text-muted"><a target="_blank" href="http://www.parliament.uk/site-information/copyright/" target="_blank">&copy; Parliamentary Copyright</a></div>
        </footer>
    </div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/assets/js/lib/jquery-1.11.0.min.js"><\/script>')</script>
<script src="/assets/js/bundle?v=jWu4Pd5MhLKe27wloAFDTSqarxAawxa3udmYu8if-rg1"></script>








    <script type="text/javascript">
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-15845045-1', 'auto');
        ga('send', 'pageview');
    </script>

</body>
</html>
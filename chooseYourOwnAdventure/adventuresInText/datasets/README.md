# Datasets

This folder contains copies of the datasets used in the worked examples and a couple of extra datasets that you might want to explore yourself.

 * [InvestigatoryPowersBill-20160606-1](InvestigatoryPowersBill-20160606-1), [InvestigatoryPowersBill-20160606-2](InvestigatoryPowersBill-20160606-2) and [InvestigatoryPowersBill-20160607](InvestigatoryPowersBill-20160607) taken from the main House of Commons debate on the Investigatory Powers Bill, [1st programme on 6th June](https://hansard.parliament.uk/Commons/2016-06-06/debates/16060613000001/InvestigatoryPowersBill), [2nd programme on 6th June]("https://hansard.parliament.uk/Commons/2016-06-06/debates/16060613000002/InvestigatoryPowersBill(Programme)(No2)") and [on the 7th June](https://hansard.parliament.uk/Commons/2016-06-07/debates/16060732000001/InvestigatoryPowersBill)
 * [iot_uk_final_organisation_160317_with_locations.csv](iot_uk_final_organisation_160317_with_locations.csv), the initial dataset of [Internet of Things companies in the UK](https://datamillnorth.org/dataset/iotuk-nation-database) compiled by IoTUK and ODI Leeds
 * [ElitistBritain-Annex.xlsx](ElitistBritain-Annex.xlsx), a set of names of people in key roles in the UK.  Taken from [this Government report on how elitist Britain is](https://www.gov.uk/government/publications/elitist-britain)
 * [ukmakerspacesidentifiabledata.csv](ukmakerspacesidentifiabledata.csv), the dataset from [NESTA's survey of makerspaces across the country](http://www.nesta.org.uk/uk-makerspaces-data)

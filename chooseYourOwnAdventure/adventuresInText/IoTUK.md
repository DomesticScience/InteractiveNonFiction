IoTUK and ODI Leeds collaborated to build a dataset of companies in the UK working in the Internet of Things.  We'll do some processing of it to aggregate the revenues of all the companies by location.

First off we'll use `wget` to download a copy of the dataset.

```
  wget https://files.datapress.com/leeds/dataset/iotuk-nation-database/2017-01-26T18:50:00/final_organisation_160317_with_locations.csv
```

And then rename it just so when we trip over the file at some point in the future we'll remember it was related to IoTUK.

```
  mv final_organisation_160317_with_locations.csv iot_uk_final_organisation_160317_with_locations.csv 
```

Now use `head` to print out the first few lines from the file, so we can see the list of column headings and a bit of the data to get a feel for it.

```
  head iot_uk_final_organisation_160317_with_locations.csv 
```

While a CSV file has `,` characters to separate each field, it's also possible (particularly when you've got a dataset containing addresses) that you'll get some commas *inside* the fields themselves.  The CSV format avoids this being a problem by enclosing the fields in `"`.

If you're using some softare that handles CSV files that won't be a problem, but because we're doing some quick-and-dirty wrangling of it as pure text it might catch us out.

For this dataset it's unlikely there'll be any additional `"` characters, so we'll use that as our field separator instead.

Eyeballing it, it looks like it'll be the fifth field that'll give us the city.  Let's use `cut` to pull that out, but we'll also add a `-` after the field number so `cut` will give us the rest of the line while we narrow in on what we're after.

```
  head iot_uk_final_organisation_160317_with_locations.csv | cut -f 5- -d \"
```

Looking at the output from that, we can do a bit better if we take the sixth field, rather than the fifth - that'll strip off a bit more of the cruft.

```
  head iot_uk_final_organisation_160317_with_locations.csv | cut -f 6- -d \"
```

Now we can work out where the turnover and company name fields are.  Looks like they'll be around the twentieth field...

```
  head iot_uk_final_organisation_160317_with_locations.csv | cut -f 6,20 -d \" 
```

...that gives us the company name as well as the city.  Let's find the turnover field...

```
  head iot_uk_final_organisation_160317_with_locations.csv | cut -f 6- -d \"
```

...yes, it's just before the company name.

```
  head iot_uk_final_organisation_160317_with_locations.csv | cut -f 6,19 -d \" 
```

That looks like the turnover column pulled out okay.  Now pull all three of those fields together.

```
  head iot_uk_final_organisation_160317_with_locations.csv | cut -f 6,19,20 -d \" 
```

Now that we've worked out what we want to extract, we'll use `cat` rather than `head` in order to pull out the data for the entire dataset.

```
  cat iot_uk_final_organisation_160317_with_locations.csv | cut -f 6,19,20 -d \" 
```

If we pass that through `sort` we'll get them ordered by location.

```
  cat iot_uk_final_organisation_160317_with_locations.csv | cut -f 6,19,20 -d \" | sort
```

A quick diversion... can we see how many companies there are for each location?

Let's jump back to just pulling out the location field, and sort that output...

```
  cat iot_uk_final_organisation_160317_with_locations.csv | cut -f 6 -d \" | sort
```

Then piping that into `uniq` we can get rid of any duplicates and `-c` will output a count too.

```
  cat iot_uk_final_organisation_160317_with_locations.csv | cut -f 6 -d \" | sort | uniq -c
```

And `sort`ing that will order it by number of companies

```
  cat iot_uk_final_organisation_160317_with_locations.csv | cut -f 6 -d \" | sort | uniq -c | sort -n
```

Okay.  Back to the turnover question.  This is where we'd got to...

```
  cat iot_uk_final_organisation_160317_with_locations.csv | cut -f 6,19,20 -d \" | sort
```

We've still got a few spurious `"` in the output.  That's because `cut` will use the same separator for its output as it used for input.  If we use the `--output-delimiter` parameter we can tell it to use something else.  Given we've also got some commas in the field output, we can get away with telling cut to use *no* delimiter, so we'll show that with single-quotes with nothing between them.

```
  cat iot_uk_final_organisation_160317_with_locations.csv | cut -f 6,19,20 -d \" --output-delimiter='' | sort
```

Let's save that as a CSV file, called "iot-companies.csv"

```
  cat iot_uk_final_organisation_160317_with_locations.csv | cut -f 6,19,20 -d \" --output-delimiter='' | sort > iot-companies.csv
```

We'll be jumping into a spreadsheet program to process the final bits of the data, and use the `=SUMIF` function there.  To help process that, we'll have a list of just the locations to search through.  We can use the commands we worked out earlier in order to a list of locations with no duplicates too...

```
  cat iot_uk_final_organisation_160317_with_locations.csv | cut -f 6 -d \" | sort | uniq
```

We can add that to the end of the CSV file with `>>` (that appends the output to the end of the file rather than replacing the file with the output)

```
  cat iot_uk_final_organisation_160317_with_locations.csv | cut -f 6 -d \" | sort | uniq >> iot-companies.csv 
```

Now we can jump into a spreadsheet to finish off our processing, but as an optional extra, we could sneak over into vim to make the text not ALL CAPS.

```
  vim iot-companies.csv 
```

Once in `vim` if you type `:g/\w/s/\<\(\w\)\(\w*\)\>/\u\1\L\2/g` it will change all of the words to "title case" - an initial capital and everything else lower case.  `:wq` will save and quit, and then we can jump into a spreadsheet to finish things off.

# Ajax 

<i>"The AJAX technique was known as Asynchronous JavaScript and XML (Ajax), because it tended to use XMLHttpRequest to request XML data. This is not normally the case these days (you'd be more likely to use XMLHttpRequest or Fetch to request JSON), 
but the result is still the same, and the term "Ajax" is still often used to describe the technique."</i>

**Chris Mills** from Mozilla has made some great tutorials, which we refer to alot that introduce API's through web and browser APIs and Javascript. It's well worth coming back to these for a deeper understanding.

 * [What is JavaScript?](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/What_is_JavaScript)
 * [Introduction to web APIs](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Client-side_web_APIs/Introduction)
 * [Fetching data from the server](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Client-side_web_APIs/Fetching_data)

I've used one of Chris' tutorials to show how you can serve data from a file and display it in html in a few ways.

 * XMLHttpRequest
 * Fetch
 * jQuery

So Ive made 
 * `shops_in_the_north.html`
 * `peopleinspaceXMLHttpRequest.html`
 * `peopleinspaceJQuery.html`
 * `peopleinspaceFetch.html`

They all do the same thing, display some data from a data file and display it on a webpage, but they all take a slightly different approach all using the very simple GET request. In `shops_in_the_north.html` it's from some local static `.txt` files I made, while the others all get JSON data from [open-notify](http://open-notify.org/) about who is on board the International Space Station but you could equally apply it to anything

You can put this html in Twine easily which we'll see later

## Further tutorials

[W3 Schools on AJAX](https://www.w3schools.com/xml/ajax_intro.asp)


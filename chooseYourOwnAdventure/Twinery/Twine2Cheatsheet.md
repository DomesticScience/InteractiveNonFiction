### Twine Cheat-sheet

### Passage text and Basic Markup

Similar to git markup Twine handles formatting text a certain way

Often, you'd like to apply styles to your text – to italicize a book
title, for example. You can do this with simple formatting codes that
are similar to the double brackets of a link. Here is what's available
to you:

Styling|Markup code|Result|HTML produced
---------------------|---------- | ---------------------- | ------------------ 
Italics | `//text//` | *text* | `<i>text</i>`
Boldface | `''text''` | **text** | `<b>text</b>`
Deleted/spoiler text | `~~text~~` | ~~text~~ | `<del>text</del>`
Emphasis | `*text*` | *text* | `<em>text</em>`
Strong emphasis | `**text**` |  **text** | `<strong>text</strong>`
Superscript | `meters/second^^2^^` | meters/second^2^ | `meters/second<sup>2</sup>`

#### Example usage: {#example-usage-}
```
    You //can't// be serious! I have to go through the ''whole game''
    again? ^^Jeez, louise!^^
```

A full guide to markup for both StoryFormats is below
 * [SugarCube Markup](http://www.motoslave.net/sugarcube/2/docs/markup.html)
 * [Harlowe Markup](https://twine2.neocities.org/)

Plain text typed into a passage is simply printed out on that passage page. There's all kinds of ways of changing that text with Twine Markup.  What's really exciting is that you can put any `<html></html>` you want in your passage

### Using `html` tags 

#### links
`<a target="_blank" href="http://tv.giphy.com/cats">Lots of Cats</a>`

#### emails
`<a href="mailto:someone@example.com?Subject=Hello%20again">Send Mail</a>`

#### adding images
`<img src="http://bookhaven.stanford.edu/wp-content/uploads/2013/08/Borges2.jpg" width="300">`

#### Complete web pages as a web `<object></object>`

This embeds another page of html withing the tags. You'll need to think carefully about the size relative to what you want displayed, although you can scroll around inside the embedded page

`<object data="https://libraryofbabel.info/search.html" width="800" height="400">  Error: Embedded data could not be displayed. </object>`

#### Use html links in arrays

You could even make a prompt for a string of text like in this [library example](http://domesticscience.org.uk/library.html) which then stores it in a variable as an array into an `<a href="">` link, so when prompted for your book title in that example add:

`<a href="https://babelia.libraryofbabel.info/imagebookmark2.cgi?tenyearsafter">Hippie Album Cover</a><!--For example-->`

#### Other Basic Code for Twine: AKA TwineScript

#### Passage Link markup

Like hyperlinks, passage links are the player's means of moving between passages and affecting the story. They consist of *link text*, which the player clicks on, and a *passage name* to send the player to.

Inside matching non-nesting pairs of `[[` and `]]`, place the link text and the passage name, separated by a 'pipe' '|'.

##### Example usage:

```
    [[Go to the cellar|Cellar]] is a link that goes to a passage named "Cellar".
    [[Parachuting|Jump]] is a link that goes to a passage named "Parachuting".
    [[Down the hatch]] is a link that goes to a passage named "Down the hatch".
```


#### The `(set: )` macro
#### `<<set expression>>`
`<<set $Variable to [...VariableToValue]>>` 
`(set: $Variable to [...VariableToValue])`

Stores data values in variables.
##### Example usage:
```
<<set $favouriteFood to "Pizza">>
(set: $favouriteFood to "Pizza")
<<set $gold to 10>>
(set: $gold to 10)
//also using standard Javascript operators
<<set $gold = $gold + 5>>
(set: $visited = true)
<<set: $visited = true>>
(set: $battlecry to "Save a " + $favouritefood + " for me!")
<<set $cheese = "Tasty" + " on " + $favouritefood>>

```
You can also set something to `true` or `false` and then use the variable to show the text in a  hook `[ ]` if it's `true`

```
<<set $isAWizard to true)
(set: $isAWizard to true)
<!--Will show the text in the [ ] if $isAWizard is true-->
$isAWizard[You wring out your beard with a quick twisting spell.]
You step into the ruined library.
$isAWizard[The familiar scent of stale parchment comforts you.]
```


#### The `(print: )` macro
`(print: Any) → Command`

This command prints out any single argument provided to it, as text.
##### Example usage:
```
<<print $gold>>
(print: $var + "s")
```
Details:

This command can be stored in a variable instead of being performed immediately. Notably, the expression to print is stored inside the command, instead of being re-evaluated when it is finally performed. So, a passage that contains:
```
<<set $name to "Duckula">>
(set: $name to "Dracula")
<<set $p to <<print: "Count" + $name>>>>
(set: $p to (print: "Count " + $name))
```

will still result in the text `Count Dracula`. This is not particularly useful compared to just setting `$p` to a string, but is available nonetheless.


#### The `<<if>> (if: )` macro
`(if: Boolean) → Changer`
`<<if: Boolean>> → Changer`

This macro accepts only booleans, and produces a command that can be attached to hooks to hide them "if" the value was false.
##### Example usage:
```
<<set $legs to 8>>
(set: $legs to 8)
<<if: $legs is 8>>You're a spider!
(if: $legs is 8)[You're a spider!]

<<if $affection eq 75>>
    I love you!
<<elseif $affection == 50>>
    I like you.
<<elseif $affection == 25>>
    I'm okay with you.
<<else>>
    Get out of my face.
<</if>>
```

#### The `(else-if: )` macro
`(else-if: Boolean) → Changer`

This macro's result changes depending on whether the previous hook in the passage was shown or hidden. If the previous hook was shown, then this command hides the attached hook. Otherwise, it acts like `(if:)`, showing the attached hook if it's `true`, and hiding it if it's `false`. If there was no preceding hook before this, then an error message will be printed.

##### Example usage:
```
Your stomach makes {
(if: $size is 'giant')[
    an intimidating rumble!
](else-if: $size is 'big')[
    a loud growl
](else:)[
    a faint gurgle
    ]}.
```



#### Good Luck out there!



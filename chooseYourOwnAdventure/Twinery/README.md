# Twine

We've been using [Twine](http://twinery.org) in it's various versions for a good while now.

As non-programmers it's a really easy way to get something that feels like a game or a complete world going. Plus as it basically outputs `html` it's easy to customize or hack it's built in CSS without having to delve too deeply into the twine engine. This means you can embed any `html` tags right in the middle of your games text, like `<object></object>`

It's also easy enough to make games with other people, young and old and with very little computer literacy. So as an artist or creative  running workshops or doing documentation or even co-design, it's a really useful tool.

It's also a gateway into simple programming in that you can use the markup language to do basic, or even quite complex logic, helping you build simple puzzles all the way up to inventories and fighting systems.

### `(set: $my_game_is_awesome to true)`

It's telling, that it's quite well used at gamejams where people hack together game prototypes, some of which end up as relatively *big* indie games.

We were able to add some custom scripts to make it work in our arcade cabinets and control it via keypresses that the arcade buttons trigger via an arduino.

So we'd like to introduce it briefly and then explore some ways of displaying and reacting to data in Twine by adding javascript in the actual passages of our Twine stories. We've seen how you can extract text from the data we are interested in and it's pretty straightforward to see how you could place that data into the games content like [Domestic Science made for the arcade](http://domesticscience.org.uk/InF.html#games).

I made a Twine tutorial for some artists and data researchers at a FACT event [Theatre Of Measurement](http://www.fact.co.uk/whats-on/current/the-theatre-of-measurement) to help make [speculative fiction around data](http://domesticscience.org.uk/games/ANewObservatory.html) which introduces what you can make and how and we'll do that together.

What is more interesting is being able to place changing or dynamic data into a Twine game or indeed any webpage or output. We'll look at a few ways of doing that with simple `<object>` tags and more creatively  `[Javascript](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/What_is_JavaScript)`, `[jQuery](https://jquery.com/)` and `[node-red](https://nodered.org/)`

## Simple Data access in Twine: Quick and dirty
### Get some simple `JSON` and display it

Twine parses any `html` tags like `<img>`, `<a href="#">` `<script>` wherever they are in a Twine passage 

We are going to use some `AJAX` basics to grab some JSON from two easily accessible sources, [open-notify](http://api.open-notify.org) and [Openweathermap](http://openweathermap.org). As we've seen in our earlier [Adventures in Text](#) it's not always that easy to find the data we want but we'll use these simple example data sets to get going.

*"The AJAX technique was known as Asynchronous JavaScript and XML (Ajax), because it tended to use things like `XMLHttpRequest()` to request XML data. This is not normally the case these days (you'd be more likely to use `XMLHttpRequest()` `jQuery` or `Fetch()` to request `JSON`), but the result is still the same, and the term "Ajax" is still often used to describe the technique."*

**Chris Mills**, who I'm quoting above has made some great tutorials, which we refer to alot that introduce using web API's and data through web and browser APIs and Javascript. It's well worth coming back to these for a deeper understanding of this area and the whole browser DOM ecosystem.

 * [What is JavaScript?](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/What_is_JavaScript)
 * [Introduction to web APIs](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Client-side_web_APIs/Introduction)
 * [Fetching data from the server](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Client-side_web_APIs/Fetching_data)

First off it's worth talking about versions in Twine. Each version of Twine has its *Story Format*, generally **Harlowe**, the current default, **SugarCube** and **Snowman**, which is a bit of a blankslate for custom CSS and javascript.

We'll be using **SugarCube 2.18.0** so you'll need to change your story format in the main menu in either your application you downloaded or in the browser.

You can look deeper into the Twine documentation below.

 * [Harlowe 2.0.1](https://twine2.neocities.org/) (the current default format in browser or app which deal with Javascript differently)
 * [SugarCube 2.18.0](http://www.motoslave.net/sugarcube/2/#documentation) (what we are using today)

What we are doing works just the same in barebones `html` so feel free to look at the included [../Ajax/peopleinspaceXMLHttpRequest.html](../Ajax/peopleinspaceXMLHttpRequest.html) files in the Ajax directory.

## Using `XMLHttpRequest()`
A really simple way of accessing the sort of data we've mined. This is the most basic of requests, a `GET` request. It just reads something on a server, without changing the server content. When you submit a form to say, subscribe to something you are making a `POST` request, here you are POSTing something to the server, look elsewhere on `moz://a` for how that works. 

Open up Twine make a Start passage and then paste in this

```
<!-- We are using the <span> element here, a generic inline container
for phrasing content, which does not inherently represent anything, but means we can combine it with id to uniquely identify it, then use Javascript to replace the content of the <span id> container. It also means that we can have some standby content in case the data doesn't make it to warn us something is wrong but give the player something-->

There are <span id="number">some</span> people in space today according to <a target="_blank" href="http://api.open-notify.org">open-notify</a>

One of them is <span id="people">a human</span>

<!--In SugarCube you use enclosing <<script>><</script>> tags to insert Javascript into a passage. 
You can load Javascript globally also but we are going to place it in our `space` passage, so each time you visit it it will run the code and get the latest `JSON` data for display-->


<script>
//Update the page
updateDisplay();
                                            
// Get a random int between two ints
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
    }
//define the updateDisplay() function and pass json into it

function updateDisplay() {
    var url = "http://api.open-notify.org/astros.json";
// new request object
    var request = new XMLHttpRequest();
// GET request
    request.open('GET', url, true);
// It's JSON
    request.responseType = 'json';
    
    request.onload = function() {
    var randomPerson = getRandomInt(0, this.response.people.length);
    document.getElementById("number").innerHTML = ("<strong>") + this.response.number + ("</strong>");
    document.getElementById("people").innerHTML = ("<strong>") + this.response.people[randomPerson].name + ("</strong>");
    }
    
    // send the data
    request.send();
    };
    
updateDisplay();
</script>
```


## Using jQuery
`[jQuery](https://jquery.com/)` has the motto **"write less, do more"**, the programmers mantra of laziness. It essentially simplifies the code we used above and will only work if you load it either from your own server or one elsewhere with `<script src="jquery.js"></script>`. Twine already uses it so we can just run `$.ajax({});` easily. 

Open up Twine make a Start passage and then a link to it like this

`[[people_in_space|space]]`

This will open up a new passage called `space`

Edit it and type this in

```
<h3>People in Space using <code>XMLHttpRequest()</code></h3>
There are <span id="number">some</span> people in space today according to <a target="_blank" href="http://api.open-notify.org">open-notify.</a>

One of them is <span id="people">a human</span>

<<script>>
//ajax command; we are using 
//comments here to explain 
//everything

$.ajax({
        // we are using json formatted data
        dataType: "json",
        //location or endpoint of the data
        url: "http://api.open-notify.org/astros.json",
        //success indicates you've accessed the endpoint and if so pass the result into a function
        success: function( result ) {
            // here we refer to <span id=number> to insert html at that point; we make it bold with <strong>, 
            //then place the data from the result array key called 'number' in the JSON using dot notation
            $( "#number" ).html( "<strong>" + result.number + "</strong>");
            // here we refer to <span id=people> to insert html at that point; we make it bold with <strong>, 
            //then place the data from the result array key called 'people' which has it's own array, and take the first item in that array, always called 0, ie result.people[0] 
            //then find the 'name' key at that position all from our JSON
            $( "#people" ).html( "<strong>" + result.people[0].name + "</strong>" + ".");
            }
        });
<</script>>
```

And from there you can do all kinds of logic in javascript

Although this works, because it's only loaded each time Twine parses the passage, it means the data we display is not accessible in the rest of the game which limits things a little. But it is a way to populate passages with all kinds of real data

## API magic myth

One thing worth noting is that although it feels like you're harnassing the power of open data and machine autonomy every API is made and maintained by humans. Open Notify's developer manually monitors NASA news to work out who is on board, then updates his JSON accordingly, but at first glance it looks like he's got a hotline to the ISS which only NASA and amateur radio enthusiasts actually have. 

Humans are always in the mix more than you think. Twine itself depends on a tonne of people maintaining it as a service so that we can use it and I always think we should keep that in mind when we talk about how we work with data especially in these times of the rise of "FAKE NEWS"

## Next up

We'll be going to use node-red with Twine next which you'll need Dave Mee's resources from the [TwineNodered](https://github.com/davemee/TwineNodered) repo and look at some of the resources in our **Twinery** directory. 

### Useful Twine links

[Tools](http://pinboard.in/u:cheapjack/t:twine/t:tools)

[Games](http://pinboard.in/u:cheapjack/t:twine/t:games)

[Cracking The Code Links](http://pinboard.in/u:cheapjack/t:crackingthecode)

## Further Tools and links

Find some [examples of interactive non-fiction](https://pinboard.in/u:cheapjack/t:textadventure/t:tools/)


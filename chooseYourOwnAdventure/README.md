# Getting Started

![CPDCard1](/uploads/10c06ab3b96b5b31d84b2c21956fabe7/CPDCard1.png)

This is the start of the resources we are distributing with our [Interactive Non-Fiction](http://domesticscience.org.uk/InF.html) project. We will be pretty much working through this material together so it would make sense to clone this repository with `git` (you'll need to [install](https://git-scm.com/downloads) it) or just the download the `zip` or `tar` file and decompress it.

But the main thing is you'll be able to pick the brains of all the project contributors and some helpers from [DoESLiverpool](http://doesliverpool.com)
 
 * [Choose Your Own `html` Adventure](#choose-your-own-html-adventure)
 * [Adventures in Text: Grep-Eyeballing & cleaning up Data with Command Line Tools](#adventures-in-text-grep-eyeballing-cleaning-up-data-with-command-line-tools)
 * [Twine](https://gitlab.com/DomesticScience/InteractiveNonFiction/blob/master/chooseYourOwnAdventure/Twinery) with Javascript
 * [node-red](https://gitlab.com/DomesticScience/InteractiveNonFiction/tree/master/chooseYourOwnAdventure/node-red) with Twine

Many of you will find some of this pretty obvious but before we start using and modding [Twine](http://twinery.org) we wanted to do something very simple.

So, here's a super simple text based choose your own adventure made in plain old html. Yes, correct the internet is already Interactive non-fiction!

## Choose Your Own `html` Adventure 

Really what we are doing is going back to first principles of making work online so we can figure out *what we really want to do* with the internet and data without getting sidetracked with installing wordpress learning coding or anything like that.

So lets get some existing data with the `<object></object>` embed tag in a super basic `<html>` page any browser can read.

We all know that if you think about something you need you're more than likely going to find someone has put something on the internet about it.

And then one further, they might even present it in some kind of useful and *readable* way; readable to humans and also readable to machines.  And we'll explore that more later when we see that this latter use becomes **even more useful to humans.** and *artists*. 

Anyway we found a really useful site that tells us the [weather](http://wttr.in) and even more usefully it renders it in `ASCII` text like a good old `Text Adventure`. What's more all the resources are [shared on GitHub](https://github.com/chubin/wttr.in). It also represents the current phase of the moon really well in `ASCII` to!

We are going to just embed that in our game. Simple. Interactive Non-Fiction using other peoples hard work. We don't think that's bad; it's a way of participating with the culture of the internet, so long as you namecheck; so props to [Igor Chubin](https://github.com/chubin)!

It's simple and easy to forget you can do this; it's quite different from using Facebook or Twitter and probably *quicker*.

I'd like to say despite this celebration of ascii and ancient text games, we are not actually interested in being retro for the sake of it.

We just want to re-think the ways we can use network culture through participation rather than use; besides our 'use' of services like Facebook, Twitter and YouTube are actually reversed; we are being used in exchange for some cool communication tools, but it comes at a cost; ultimately *we* become the *product*.

We think by getting back to simpler forms of participation we can be more creative and more resilient. But we aren't crusaders or purists; you can do some really cool stuff with Twitter (& dark Trumpy stuff of course) that doesn't fit the user as product story, where you can control what it can do and say and make [art](https://twitter.com/predartbot).

Some of what we will be doing will give you basics in engineering this kind of thing online.

## Adventures in Text: Grep-Eyeballing & cleaning up Data with Command Line Tools

So first principles in the Web with `html`, so we are going to look at first principles with data. Just what sort of data is on the internet?

[@amcewen](https://twitter.com/amcewen) has made some resources and activity to explore this which you can find in this repo.

We are going to use some powerful text manipulation and search tools on the command line terminal. It will help you search for the key words or numbers or symbols you want from a dataset. **Excel** and other spreadsheet programmes and text editors can do this but often in limited ways. These tools exist by default on most Linux & Unix systems like Raspberry Pi's and many servers. They are the workhorses of the internet so getting to know their tools is really useful. 

If you have never used the command line before you can refer to your **CheatSheetPrint** and **CmdLineCheatSheet** for more detail. The command line is like the original `terminal` from when computers where the size of rooms and users would access it from a computer monitor with no graphics. It's also where the Text Adventure was born!

This is where Windows users will need to install `cygwin`

### Types of data

Underlying any snazzy infographic will be some basic formats of data that various components can parse into something readable. We will be looking at a few of the most common and simple formats:

### `JSON`
An array of data with paired *names* and *values* that can be a string of text, numbers or a Boolean (`true` or `false`)

```
 var myJson = [
 {
    "first": "John",
    "last": "Doe",
    "age": 39,
    "sex": "male",
    "salary": 70000,
    "registered": true
    },
 {
    "first": "Jane",
    "last": "Smith",
    "age": 42,
    "sex": "female",
    "salary": 80000,
    "registered": true
    },
    ];
```

### `.txt` 
A Simple list or string of characters with line endings `\n`

### `.csv`
A list of **C**omma **S**eperated **V**alues often with a header and similar to a spreadsheet

```
street,city,zip,state,beds,baths,sq__ft,type,sale_date,price,latitude,longitude
3526 HIGH ST,SACRAMENTO,95838,CA,2,1,836,Residential,Wed May 21 00:00:00 EDT 2008,59222,38.631913,-121.434879
51 OMAHA CT,SACRAMENTO,95823,CA,3,1,1167,Residential,Wed May 21 00:00:00 EDT 2008,68212,38.478902,-121.431028
```

### `.xls` 
This is just a spreadsheet format of what is essentially a `.csv` file

### `.xml`
An older format of text still referenced in `XMLHttpRequest()`

### `.pdf`
Adobe's Knightmare of a proprietary format. Eminently tricky to read. Way too much complicated formatting. We won't be using it. There are things that can help like [pandoc](http://pandoc.org/) but we won't have time to look at it

## Data is Dirty

In an ideal world we look for some data to help tell a story, do some searching with Google or DuckDuckGo and find it perfectly ready for us. For my **chooseYourOwnAdventure** example I wanted a list of all shopping centres but at first nothing out there seemed to be a fit.

Apart from the dangers of a pick & mix approach to browsing for data just how the data is organised can be you're most immeadiate problem.

You might find something in a government open data resource or just taken from an existing html page, but it will have data or columns or formats you dont need so you'll need something to search, filter and make new data files. In cmdline language you need to `grep`, `sort` and then `pipe`, `>`(output to file) and `>>`(append to file).

A potential problem is that you could become biased to well formatted data, when more relevant or more accurate data is buried inside an elaborate JSON database; we want to sign post tools so you will be undaunted by format and get to what you need.

Back in our html adventure I found my list of shops from just viewing a web page's source code after a quick search (any browser lets you do that by pressing `cmd` & `U`) and just removing everything except the shop names. This wasn't comprehensive at all; what would have been better where council retail estate data but for my example it was *good enough* data. I used 

Follow [@amcewen](https://twitter.com/amcewen) workshop in the [Adventures in Text](adventuresInText) directory and we'll end up with some data we can play with later in Twine.


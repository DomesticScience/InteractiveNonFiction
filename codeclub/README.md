## Twine for Code Club Liverpool


### Example Games

 * [The Adventures of Barry](http://liverpoolcodeclub.org/projects/twine/the-adventure-of-barry.html)
 * [Austerity In the Home](https://domesticscience.org.uk/games/AusterityInTheHome.html) Mundane shopping and neoliberal ideology
 * [Northern Powerhouse](http://textadventuretime.co.uk/play.html) Epic game made by 16-19 yr old residents of Wigan, Hull and Blackburn
 * [Reduced To Clear](https://domesticscience.org.uk/games/ReducedToClear.html) A Supermarket in the 90s
 * [These British Aisles](https://domesticscience.org.uk/games/BritishAisles/These_British_Aisles.html) Wiki of bad British Food
 * [Tocky Tales](http://planning.mcqn.com/) Game set in Toxteth Library
 * [The Walter Benjamin App Game](https://domesticscience.org.uk/WorksOfArt.html) arty game about the philosopher Walter Benjamin
 * [Wray Castle Text Adventure](https://domesticscience.org.uk/wray.html) Game set in an old castle in the Lake District 
 * [You Will Select A Decision](https://selectadecision.info/) 

### Old Games

 * [Lost Frog](http://bbcmicro.co.uk/game.php?id=2156) The classic BBC text adventure by legendary Maths teacher [Anita Straker](https://en.wikipedia.org/wiki/Anita_Straker) we often make kids play it to get the vibe

### Useful links

 * [Librarian Cheatsheet](https://github.com/cheapjack/ForkingLibraries/blob/master/workshop/cheatsheet.md) Some twine basics and logic
 * [Cheatsheet](https://gitlab.com/DomesticScience/InteractiveNonFiction/blob/master/CheatSheetPrint.md#twine) Some basics and logic plus command line tool guides for inspecting text data
 * [Data Games](https://gitlab.com/DomesticScience/InteractiveNonFiction/blob/master/ExampleGames.md) 
 * [Harlowe Manual](https://twine2.neocities.org/) Harlowe is the default format / style for twine 

### Tutorials

 * [Twine Cookbook](https://twinery.org/cookbook/) The main cookbook for doing stuff in twine
 * [Zarino's Code Club Intro](http://liverpoolcodeclub.org/projects/twine/) Zarino from Liverpool Code club made a really simple and thorough tutorial in the code club format

### Non Twine

 * [AdventureOn](https://adventuron.io/) For those hardcore types who want to make *proper* text adventures
 * [AdventureOn BETA](https://adventuron.io/legacy/ide/058/) For the hardcore who cant be bothered with the tutorials for AdventureOn

# Example Games & Tutorials

Play and learn some of the games we're working on, those that have inspired us and some tools and resources to take things further

### Inspiring Games

Some games based on data that have inspired us.

 * [UK PM](http://bbcmicro.co.uk/game.php?id=1618)
 * [Has Theresa May Resigned Yet?](http://hastheresamayresignedyet.com/)
 * [1984](http://bbcmicro.co.uk/game.php?id=937)
 * [Travel from Glasgow to London as a Wheelchair User: The Text Adventure](https://www.buzzfeed.com/louiseridley/can-you-travel-from-glasgow-to-london-as-a-wheelchair-user)
 * [Command Line Murder Mystery](https://github.com/veltman/clmystery)

### Domestic Science Games

Games made by us, mainly in [Twine](http://twinery.org)

 * [Beba-me💧 Drink Me](http://drinkme.textadventuretime.co.uk/)
 * [Code For Liverpool](http://domesticscience.org.uk/codeforliverpool.html)
 * [Claim Asylum In The Uk: The Text Adventure](https://thimbleprojects.org/HTTP/cheapjack/106310/)
 * [Austerity In The Home](http://domesticscience.org.uk/games/AusterityInTheHome.html)
 * [These British Aisles](http://domesticscience.org.uk/games/BritishAisles/These_British_Aisles.html)
 * [Reduced To Clear](http://domesticscience.org.uk/games/ReducedToClear.html)

## Tutorials

Handy Tutorials for further research

 * [Mozilla Developer Network: Learn Web Development](https://developer.mozilla.org/en-US/docs/Learn)
 * [Bot Tutorials](https://botwiki.org/tutorials/)
 * [More Tutorial links](https://pinboard.in/u:cheapjack/t:InF/t:tutorials)



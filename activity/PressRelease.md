# Interactive Non Fiction Arcade
## Creative Professional Development Workshops

 * [Friday 6th October 2017 10:00-15:30](https://www.eventbrite.co.uk/e/interactive-non-fiction-creative-professional-development-workshops-tickets-37122460278)
 * [Saturday 7th October 2017 10:00-15:30](https://www.eventbrite.co.uk/e/interactive-non-fiction-creative-professional-development-workshops-2-tickets-37728123834)

<img src="https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F34786311%2F4083925759%2F1%2Foriginal.jpg?w=800&rect=0%2C718%2C2306%2C1153&s=4ba38e910dc0c7b2c90100ad25021810" width="600">

Inspired by the evolution of [Text Adventures](http://www.textadventuretime.co.uk/2015/05/28/hit-troll-with-axe-a-zork-review/) into *Interactive Fiction* and the game arcades of the past, **Interactive Non-Fiction**, is a way of making text based games and other activity that use data from the Internet and the Real World.

We've been touring our exhibition of text based artworks and games installed in free standing Arcade Cabinets so that people can play these games in a more social way like the arcades and fairgrounds of the past pioneered.

We've been across the Northwest at Toxteth Library, Barrow Library and The Exchange CIC in Morecambe's West End before returning to Liverpool.

These simple games may seem retro or limited compared to some of the amazing possibilities of `html`, data and the web but making them with free open-source tools like [Twine](http://twinery.org) can be the first step in understanding how to use the web creatively 

We think there are intermediate skills missing for artists and enthusisasts to make this kind of work so we're working with local creative technologists to put together a full day technical course for **£25** with artists and technologists at *[Toxteth Library](https://liverpool.gov.uk/libraries/find-a-library/toxteth-library/)*

You'll get some great [reference materials](https://gitlab.com/DomesticScience/InteractiveNonFiction) and hands on experience for building interactive work based on the diverse data on the internet using existing websites, APIs and a wealth of contemporary web resources. We think the internet *already is*
interactive non-fiction, it's just easy to get sidetracked out there. We think artists and other story tellers need to use it in a more contemporary way that reflects technical culture as it is now.

### Play Games!

You can <a style="background-color: rgb(10, 255, 74); padding: 0.5%; color: rgb(0,0,0);" href="http://domesticscience.org.uk/InF.html#games">play the artworks and games in the arcade here</a> 

**Glenn Boulter's** game, **Reduced To Clear** is one of the first game in the arcade which you can play online below and uses data from the [Office of National Statistics](https://www.ons.gov.uk/), **Hwa Young Jung's** game is like a `wiki` of British Shopping habits while **Ross Dalziel's** game **Austerity In The Home** is about belief in politics, dodgy data analogies and an advert for our workshops..</p>

### Buy Tickets!
Available from the links below:
 * [Friday 6th October 2017](https://www.eventbrite.co.uk/e/interactive-non-fiction-creative-professional-development-workshops-tickets-37122460278)
 * [Saturday 7th October 2017](https://www.eventbrite.co.uk/e/interactive-non-fiction-creative-professional-development-workshops-2-tickets-37728123834)

[**Please complete our survey**](https://goo.gl/forms/DL44adyLH1dXZrat2 "Survey") if you haven't already it will help us know your experience and interests and usual operating system!

### Further Information

Further Information can be found on the [Domestic Science website](http://domesticscience.org.uk/InF.html) and at the [projects code repository](https://gitlab.com/DomesticScience/InteractiveNonFiction)



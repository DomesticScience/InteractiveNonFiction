# Build An Arcade For Twine

We used lasercut cabinets made by James Medd using [these components](#)

Then the Pi serves the games in Chromium and uses an arduino Leonardo to act as a USB keyboard. See the arduino code.

## Keeping cabinets Updated

There is another [non-fiction](https://github.com/cheapjack/nonfiction) with different branches for different cabinets as part of different projects, like the [Arcade De Bruno Latour](https://domesticscience.org.uk/criticalkits/InF.html), which means you can update the repo and feed in updates remotely etc.


## Building cabinets

[James Medd]() designed the cabinets and you can see the cut files in the cabinets/ directory


## Raspberry Pi

We use the latest Raspbian disk img and chromium to display the games in a `game` folder in your `/home/pi` folder

## Kiosking Chromium
Here's what the [Awkward Arcade](#) and [Interactive Non Fiction](#) projects use. 

## Display
Use `unclutter`  to hide the mouse etc.
`sudo apt-get install unclutter`

## Autostarting games on Pi
Make an `autostart` file with no extension and place it here `/home/pi/.config/lxsession/LXDE-pi/autostart`

Swap out the location of your first game or make an `html` menu that loads first.

```
@lxpanel --profile LXDE-pi
@pcmanfm --desktop --profile LXDE-pi
#@xscreensaver -no-splash

@xset s off
@xset -dpms
@xset s noblank
@unclutter -idle 0.1 -root
/usr/bin/chromium-browser --incognito --disable-restore-session-state --kiosk --app /home/pi/games/game.html
```

## Twine control
![Control Panel](https://gitlab.com/DomesticScience/InteractiveNonFiction/uploads/08bc31276584f69d791b1db00eeb22d2/quickLayout.png)

### Key Mapping


```
          UP ('218') - BLUE
           
           LEFT/PREV LINK - RED        RIGHT/NEXT LINK - GREEN     ENTER/SELECT - WHITE      
           ('Tab Left', '179'          ('Tab', '179')              ('176')
           + 'KEY_LEFT_SHIFT')

                   DOWN('Down', '217') - YELLOW
                   
``` 
```
MENU keycombo ( shift+Key m, 'KEY_LEFT_SHIFT' 'M' ) - BLACK
```
#### Behaviour: 

`LEFT` & `RIGHT` `tab` & `shift+tab` through the links `PREV/NEXT`, `UP` & `DOWN` scroll the html pages, SELECT is enter and mapped to '176'  and `MENU` is mapped to opening a specific html file, in our case a menu screen 

 *  White & black buttons RIGHT of direction keys
 * `SELECT/FIRE - WHITE`
 * `MENU - BLACK`

We use an arduino Leonardo running this code `twineControl.ino` 

```
// Simple Keyboard mapping

#include <Keyboard.h>

char shiftKey = KEY_LEFT_SHIFT;
char tabKey = KEY_TAB;

void setup() {
  // make pins 2-5 inputs and turn on the 
  // pullup resistor so it stays high unless
  // pulled to ground:
  // 2 is Blue (UP, '218')
  pinMode(2, INPUT_PULLUP);
  
  // 3 is Black for menu, keycombo ( shift+Key m, 'KEY_LEFT_SHIFT' 'M' )
  pinMode(3, INPUT_PULLUP);
  
  // 4 is Yellow ('Down', '217')
  pinMode(4, INPUT_PULLUP);
  
  // 5 is White ('Select', 'Emter', '176')
  pinMode(5, INPUT_PULLUP);
  
  // 6 is Red for shift+Tab, keycombo ('Tab', '179' + 'KEY_LEFT_SHIFT') )
  pinMode(6, INPUT_PULLUP);
  
    // 8 is Green for Tab ('Tab', '179')
  pinMode(8, INPUT_PULLUP);  
  Keyboard.begin();
}

void loop() {
  //if the button is pressed
  if(digitalRead(2)==LOW){
    //Send the message UP
    Keyboard.write(218);
    delay(250);
  }
  else if (digitalRead(4)==LOW){
    //Send the message DOWN
    Keyboard.write(217);
    delay(250);
  }
  else if(digitalRead(5)==LOW){
    //Send the message
    Keyboard.write(176);
    delay(250);
    }
// use Keyboard.press to do a key combo
  else if (digitalRead(3)==LOW){
    Keyboard.press(shiftKey);
    Keyboard.press('m');
    delay(250);
    Keyboard.releaseAll();
    delay(1000);    
  }
  else if (digitalRead(6)==LOW){
    // Send the message 'shift+KEY_TAB'
    Keyboard.press(shiftKey);
    Keyboard.press(tabKey);
    delay(250);
    Keyboard.releaseAll();
    delay(250);
  }
  else if (digitalRead(8)==LOW){
    // Send KEY_TAB
    Keyboard.write(tabKey);
    delay(250);
  }
    else
    {
      delay(250);
    }

  }
```

## Twine control
The arduino fakes a USB keyboard for the Pi because GPIO keymapping became difficult. Contributions welcome!

### Twine 1.4.2

Save this as `twine1Nav.js` in your game folder and load it in after the main twine engine

`<script src="twine1Nav.js"></script>`


```
// Javascript navigation code for Twine 1.4.2
// By James Medd

setTimeout(function() {
  var internalLinks = document.getElementsByClassName('internalLink');
  for (var i = 0; i < internalLinks.length; i ++) {
	  internalLinks[i].href = "#";
  }
  internalLinks[0].focus();
  var target = document.getElementById("passages");
  var observer = new MutationObserver(function(mutations) {
    console.log("Change");
    setTimeout(function() {
    var internalLinks = document.getElementsByClassName('internalLink');
    for (var i = 0; i < internalLinks.length; i ++) {
  	  internalLinks[i].href = "#";
    }
    internalLinks[0].focus();
  }, 1000);
  });
  var config = { attributes: true, childList: true, characterData: true };
  observer.observe(target, config);
  function keySelectOption(inputs) {
    for (var i=0; i<inputs.length; i++) {
      document.onkeydown = function(e) {
      var option = document.activeElement;
      if (e.keyCode==40) {
        var node = option.nextSibling;
        while (node) {
          if (node.tagName=='A') {
            node.focus();
            break;
          }
          node = node.nextSibling;
        }
      }
      else if (e.keyCode==38) {
        var node = option.previousSibling;
        while (node) {
          if (node.tagName=='A') {
            node.focus();
            break;
          }
          node = node.previousSibling;
        }
      }
// extra code to trigger menu
      else if (e.code == "KeyM" && e.shiftKey){
        window.location.href = "location-of-menu.html";
	};
    };
  };
}
keySelectOption(document.getElementsByTagName('a'));
}, 1000);
```

### Twine 2 Harlowe 1.2.4 

Save this as `twine1Nav.js` in your game folder and load it in after the main twine engine

`<script src="twine2Nav.js"></script>`

```
// jQuery navigation code for Twine 2 Harlowe 1.2.4
// By James Medd & @cheapjack
// 
//wait a second and select the first tw-link on the page
jQuery(document).ready(function() {
  setTimeout(function(){jQuery('tw-expression tw-link:first').focus()}, 1000);
});

//select tw-story node
var target = document.getElementsByTagName("tw-story")[0];

// create an observer instance, get it to execute a function whenever tw-story changes (i.e. when the user interacts)
var observer = new MutationObserver(function(mutations) {
  setTimeout(function(){jQuery('tw-expression tw-link:first').focus()}, 1000)
});

// configuration of the observer:
var config = { attributes: true, childList: true, characterData: true };

// pass in the target node, as well as the observer options
observer.observe(target, config);

//bind left and right keypresses to link navigation
jQuery(document).keydown(function(e){
	if (e.which == 38) {
    jQuery('tw-link:focus').parent('tw-expression').prev().find('tw-link').focus();
  }
  else if (e.which == 40) {
    jQuery('tw-link:focus').parent('tw-expression').next().find('tw-link').focus();
  }
// extra code to trigger menu, note change of order and handler :)
// change the location of a menu or game to get back to, comment out if not required
  else if (e.shiftKey && e.which == 77){
    window.location.href = "location-of-menu-file/or/game.html";
  }

});
```

If you make link to a new browser tab `<a target="_blank" href src="link-to-another-page-or-game.html>` you can detect keypresses to navitgate, close it and return to the original as long as you load `jQuery` into the `html`. Nice hack to interlink games or other resources in the same Chromium browser.

```
//bind left and right keypresses to link navigation
jQuery(document).keydown(function(e){
        if (e.which == 38) {
                jQuery('tw-link:focus').parent('tw-expression').prev().find('tw-link').focus();
                  }
                    else if (e.which == 40) {
                            jQuery('tw-link:focus').parent('tw-expression').next().find('tw-link').focus();
                              }
                              // extra code to trigger menu, note change of order and handler :)
  else if (e.shiftKey && e.which == 77){
          window.close();
            }

});
```


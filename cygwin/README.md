# Installing cygwin

This will install a bunch of handy "UNIX-like" (or "Linux-like") tools, which we'll be using to wrangle text more easily.

## Installation at Workshop

We've downloaded the files you'll need to make it quicker to install things during the workshop.  Follow these steps to get up and running with cygwin.  (If you're not at the workshop, skip to the [standard installation](#standard-installation) instructions instead)

1. Copy the `cygwin-install` folder and `setup-x86_64.exe` from the memory stick to somewhere on your computer.
1. Run `setup-x86_64.exe`
1. Click `Yes` to allow it to install software to your computer
1. Click `Next`
1. Choose `Install from Local Directory` and click `Next`
1. Click `Next`
1. On the `Select Local Package Directory` screen, use the `Browse` button to find where you copied the `cygwin-install` folder in step 1.
1. Click `Next`, which will then start the install
1. Click `Next` to choose the default options of what to install
1. Wait for a bit while it installs things.
1. Click `Finish` to complete the install.

## Standard Installation

1. Visit the [cygwin download page](http://www.cygwin.com/install.html)
1. Download the `setup-X.exe` for your version of Windows (if you're not sure, try `setup-x86_64.exe` first)
1. Once it has downloaded, run it. 
1. Click `Yes` to allow it to install software to your computer
1. Click `Next`
1. Choose `Install from Local Directory` and click `Next`
1. Click `Next`
1. Choose a site to download from.  Try to find one that looks local, but it doesn't particularly matter
1. Click `Next`, which will download the package list
1. Click `Next` to choose the default options of what to install
1. Wait for a bit while it installs things.
1. Click `Finish` to complete the install.

## Running cygwin

In your start menu, you should now have a `cygwin Terminal` option.
